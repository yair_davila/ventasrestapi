from rest_framework import viewsets
from ..serializer.usuarioSerializer import UsuarioSerializer
from django.contrib.auth.models import User
from drf_spectacular.utils import extend_schema

class UsuarioViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UsuarioSerializer

    @extend_schema(
        tags=['Usuarios'],
        description='Lista todos los usuarios',
        responses={
            200: UsuarioSerializer(many=True),  # Define el esquema de respuesta para el código 200
        },
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @extend_schema(
        tags=['Usuarios'],
        description='Crea un nuevo usuario',
        request=UsuarioSerializer,  # Define el esquema de la solicitud
        responses={
            201: UsuarioSerializer,  # Define el esquema de respuesta para el código 201
        },
    )
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @extend_schema(
        tags=['Usuarios'],
        description='Recupera un usuario específico',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID del usuario",
            }
        ],
        responses={
            200: UsuarioSerializer,  # Define el esquema de respuesta para el código 200
        },
    )
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @extend_schema(
        tags=['Usuarios'],
        description='Actualiza un usuario específico',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID del usuario",
            }
        ],
        request=UsuarioSerializer,  # Define el esquema de la solicitud
        responses={
            200: UsuarioSerializer,  # Define el esquema de respuesta para el código 200
        },
    )
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @extend_schema(
        tags=['Usuarios'],
        description='Elimina un usuario específico',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID del usuario",
            }
        ],
    )
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)
    
    
    @extend_schema(
        tags=['Usuarios'],
        description='Actualiza parcialmente un usuario específico',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID del usuario",
            }
        ],
        responses={
            200: UsuarioSerializer,  # Define el esquema de respuesta para el código 200
        },
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)
