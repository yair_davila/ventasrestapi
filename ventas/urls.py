from django.contrib import admin
from django.urls import include,path
from rest_framework import routers
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

from productos.view.productoViewSet import ProductoViewSet
from productos.view.carritoCompraViewSet import CarritoCompraViewSet
from productos.view.usuarioViewSet import UsuarioViewSet
from productos.view.ventaViewSet import VentaViewSet
from productos.view.homePageView import HomePageView


router = routers.DefaultRouter()
router.register(r"productos", ProductoViewSet)
router.register(r"carrito-compra", CarritoCompraViewSet)
router.register(r"ventas", VentaViewSet)
router.register(r"user", UsuarioViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path("api/", include(router.urls)),
    # Ruta para la vista de esquema OpenAPI
    path('schema/', SpectacularAPIView.as_view(), name='schema'),

    # Ruta para la vista de Swagger UI
    path('swagger/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),
    
    path('', HomePageView.as_view(), name='home'),


]


