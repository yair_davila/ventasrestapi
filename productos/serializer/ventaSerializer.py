from ..model.venta import Venta
from rest_framework import serializers

class VentaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Venta
        fields = ("id", "usuario", "producto", "cantidad", "fecha_venta")
