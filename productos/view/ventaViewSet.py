from ..model.venta import Venta
from ..serializer.ventaSerializer import VentaSerializer
from rest_framework import viewsets
from drf_spectacular.utils import extend_schema

class VentaViewSet(viewsets.ModelViewSet):
    queryset = Venta.objects.all()
    serializer_class = VentaSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        usuario = self.request.user
        return queryset.filter(usuario=usuario)

    @extend_schema(
        tags=['Ventas'],
        description='Lista todas las ventas del usuario actual',
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @extend_schema(
        tags=['Ventas'],
        description='Crea una nueva venta',
        responses={
            201: VentaSerializer,  # Define el esquema de respuesta para el código 201
        },
    )
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @extend_schema(
        tags=['Ventas'],
        description='Recupera una venta específica',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID de la venta",
            }
        ],
        responses={
            200: VentaSerializer,  # Define el esquema de respuesta para el código 200
        },
    )
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @extend_schema(
        tags=['Ventas'],
        description='Actualiza una venta específica',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID de la venta",
            }
        ],
        responses={
            200: VentaSerializer,  # Define el esquema de respuesta para el código 200
        },
    )
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @extend_schema(
        tags=['Ventas'],
        description='Elimina una venta específica',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID de la venta",
            }
        ],
    )
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)
    
    @extend_schema(
        tags=['Ventas'],
        description='Actualiza parcialmente una venta específica',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID de la venta",
            }
        ],
        responses={
            200: VentaSerializer,  # Define el esquema de respuesta para el código 200
        },
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

