from ..model.carritoCompra import CarritoCompra
from rest_framework import serializers

class CarritoCompraSerializer(serializers.ModelSerializer):
    class Meta:
        model = CarritoCompra
        fields = ("id", "usuario", "producto", "cantidad")