from ..model.carritoCompra import CarritoCompra
from ..serializer.carritoCompraSerializer import CarritoCompraSerializer
from rest_framework import viewsets
from drf_spectacular.utils import extend_schema

class CarritoCompraViewSet(viewsets.ModelViewSet):
    queryset = CarritoCompra.objects.all()
    serializer_class = CarritoCompraSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        usuario = self.request.user
        return queryset.filter(usuario=usuario)

    @extend_schema(
        tags=['Carrito de Compra'],
        description='Lista todos los carritos de compra del usuario actual',
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @extend_schema(
        tags=['Carrito de Compra'],
        description='Crea un nuevo carrito de compra',
        responses={
            201: CarritoCompraSerializer,  # Define el esquema de respuesta para el código 201
        },
    )
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @extend_schema(
        tags=['Carrito de Compra'],
        description='Recupera un carrito de compra específico',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID del carrito de compra",
            }
        ],
        responses={
            200: CarritoCompraSerializer,  # Define el esquema de respuesta para el código 200
        },
    )
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @extend_schema(
        tags=['Carrito de Compra'],
        description='Actualiza un carrito de compra específico',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID del carrito de compra",
            }
        ],
        responses={
            200: CarritoCompraSerializer,  # Define el esquema de respuesta para el código 200
        },
    )
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @extend_schema(
        tags=['Carrito de Compra'],
        description='Elimina un carrito de compra específico',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID del carrito de compra",
            }
        ],
    )
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)
    
    @extend_schema(
        tags=['Carrito de Compra'],
        description='Actualiza parcialmente un carrito de compra específico',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID del carrito de compra",
            }
        ],
        responses={
            200: CarritoCompraSerializer,  # Define el esquema de respuesta para el código 200
        },
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)
