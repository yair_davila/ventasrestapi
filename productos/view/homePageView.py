from django.views.generic import TemplateView

class HomePageView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = 'Sistema API-Rest para ventas'
        context['enlaces'] = [
            {'url': 'admin/', 'texto': 'Administración'},
            {'url': 'api/', 'texto': 'API'},
            {'url': 'schema/', 'texto': 'Esquema JSON'},
            {'url': 'swagger/', 'texto': 'Documentación Swagger'},
        ]
        return context
