from ..model.producto import Producto
from ..serializer.productoSerializer import ProductoSerializer
from rest_framework import viewsets
from drf_spectacular.utils import extend_schema

class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer

    def get_queryset(self):
        queryset = super().get_queryset()
        params = self.request.query_params
        if "nombre" in params:
            queryset = queryset.filter(nombre__icontains=params["nombre"])
        return queryset

    @extend_schema(
        tags=['Productos'],
        description='Lista todos los productos',
        parameters=[
            {
                "name": "nombre",
                "in": "query",
                "type": "string",
                "description": "Filtra los productos por nombre (búsqueda parcial)",
            }
        ],
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @extend_schema(
        tags=['Productos'],
        description='Crea un nuevo producto',
        responses={
            201: ProductoSerializer,  # Define el esquema de respuesta para el código 201
        },
    )
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @extend_schema(
        tags=['Productos'],
        description='Recupera un producto específico',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID del producto",
            }
        ],
        responses={
            200: ProductoSerializer,  # Define el esquema de respuesta para el código 200
        },
    )
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @extend_schema(
        tags=['Productos'],
        description='Actualiza un producto específico',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID del producto",
            }
        ],
        responses={
            200: ProductoSerializer,  # Define el esquema de respuesta para el código 200
        },
    )
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @extend_schema(
        tags=['Productos'],
        description='Elimina un producto específico',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID del producto",
            }
        ],
    )
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)

    @extend_schema(
        tags=['Productos'],
        description='Actualiza parcialmente un producto específico',
        parameters=[
            {
                "name": "pk",
                "in": "path",
                "type": "integer",
                "required": True,
                "description": "ID del producto",
            }
        ],
        responses={
            200: ProductoSerializer,  # Define el esquema de respuesta para el código 200
        },
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)
